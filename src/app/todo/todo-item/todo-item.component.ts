import { SelecionarTodoAction, EditarTodoAction, BorrarTodoAction } from './../../reducers/todo.actions';
import { AppState } from './../../reducers/app.state';
import { FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Todo } from 'src/app/reducers/todo.model';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styles: []
})
export class TodoItemComponent implements OnInit {

  @Input() todoItem: Todo;
  @ViewChild('txtFisico', {static: false}) txtFisico: ElementRef;

  checkFiel: FormControl;
  txtInputItem: FormControl;
  editando: boolean;
  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.checkFiel = new FormControl(this.todoItem.completado);
    this.txtInputItem = new FormControl(this.todoItem.texto, Validators.required);
    console.log(this.todoItem);
    this.checkFiel.valueChanges.subscribe(valor => {
        console.log(valor);
        const action = new SelecionarTodoAction(this.todoItem.id);
        this.store.dispatch(action);

    });

  }
  editar(){
    this.editando = true;
    setTimeout(() => {
      this.txtFisico.nativeElement.select();
    }, 1);
  }
  terminarEdition(){
    this.editando = false;
    if (this.txtInputItem.invalid) {
      return;
    }
    if (this.txtInputItem.value === this.todoItem.texto) {
      return;
    }
    const action = new EditarTodoAction(this.todoItem.id,this.txtInputItem.value);
    this.store.dispatch(action);
  }
  borrar(){
    const action = new BorrarTodoAction(this.todoItem.id);
    this.store.dispatch(action);
  }

}
