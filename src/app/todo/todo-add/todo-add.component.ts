import { AppState } from './../../reducers/app.state';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as  formTodo from './../../reducers/todo.actions';
@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styles: []
})
export class TodoAddComponent implements OnInit {
  txtinput: FormControl;
  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.txtinput = new FormControl('', Validators.required);
  }

  agregarTodos(){
    if(this.txtinput.invalid){
      return;
    }
    const action = new formTodo.AgregarTodoAction(this.txtinput.value);
    this.store.dispatch(action);
    console.log(this.txtinput.value);
    console.log(this.txtinput.valid);
    this.txtinput.setValue('');

  }

}
