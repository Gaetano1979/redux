import { BorrarAllTodoAction } from './../../reducers/todo.actions';
import { Todo } from './../../reducers/todo.model';
import { AppState } from './../../reducers/app.state';
import { Component, OnInit } from '@angular/core';

import * as fromFiltro from '../../reducers/filter/filter.action';
import { Store } from '@ngrx/store';
@Component({
  selector: 'app-todo-footer',
  templateUrl: './todo-footer.component.html',
  styles: []
})
export class TodoFooterComponent implements OnInit {

  filtroValidos: fromFiltro.filtrosValidos[] = ['Todos','Completados','Pendientes'];
  filtroActual: fromFiltro.filtrosValidos;

  pendientes: number;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.store.select('todos').subscribe(todos=>{
      this.contarPendientes(todos);

    });
    this.store.select('filtro').subscribe(filtroStore=>{
      this.filtroActual = filtroStore;
    });
  }

  cambiarFiltro(item: fromFiltro.filtrosValidos){

    const accion = new fromFiltro.SetFilterAction(item);
    this.store.dispatch(accion);

  }

  contarPendientes(todos: Todo[]){
    this.pendientes = todos.filter(todo => !todo.completado).length;
  }

  borrarTodo(){
    const action = new BorrarAllTodoAction();
    this.store.dispatch(action);

  }

}
