import { Todo } from './../../reducers/todo.model';
import { AppState } from './../../reducers/app.state';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as formfilter from './../../reducers/filter/filter.action';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styles: []
})
export class TodoListComponent implements OnInit {

  todos: Todo[] = [];
  filtro: formfilter.filtrosValidos;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.store.subscribe(state => {
        this.todos = state.todos;
        this.filtro = state.filtro;
    });
  }

}
