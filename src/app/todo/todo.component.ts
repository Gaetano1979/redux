import { SelecionarAllTodoAction } from './../reducers/todo.actions';
import { AppState } from './../reducers/index';
import { FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styles: []
})
export class TodoComponent implements OnInit {

  completado = false;
  checkTodo: FormControl;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.checkTodo = new FormControl(false);
    this.checkTodo.valueChanges.subscribe(valor=>{
      console.log(valor);
      this.completado = valor;
      const action = new SelecionarAllTodoAction(valor);
      this.store.dispatch(action);

    });
  }

  tacharTodos(){
    this.completado = !this.completado;
    console.log(this.completado);
  }

}
