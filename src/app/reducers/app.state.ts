import { Todo } from './todo.model';
import { ActionReducerMap } from '@ngrx/store';
import { todoReducer } from './todo.reducer';
import { filtroReducer } from './filter/filter.reducer';

import * as fromFiltros from './filter/filter.action';

export interface AppState{
  todos: Todo[];
  // filtro: string;
  filtro: fromFiltros.filtrosValidos;
}

export const AppReducers: ActionReducerMap<AppState> = {
  todos: todoReducer,
  filtro: filtroReducer
};


