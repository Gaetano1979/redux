import { Action } from '@ngrx/store';


export const SET_FILTRO = '[SET_FILTER] Set Filter';

export type filtrosValidos = 'Todos' | 'Pendientes' | 'Completados';

export class SetFilterAction implements Action{
  readonly type = SET_FILTRO;
  constructor(public filtro: filtrosValidos){

  }
}

export type acciones = SetFilterAction;
