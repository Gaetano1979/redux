import { Todo } from './todo.model';
import * as ActionesTodo from './todo.actions';

// Creamos una lista de Todos por defecto
const Todo1 = new Todo('Todo uno');
const Todo2 = new Todo('Todo dos');
const Todo3 = new Todo('Todo tres');

Todo2.completado = true;

// Creamos el estado inicial de Todos
const estadoinicial: Todo[] = [Todo1, Todo2, Todo3];
export function todoReducer(state = estadoinicial, action: ActionesTodo.Acciones): Todo[]{

  switch (action.type) {

    case ActionesTodo.AGREGAR_TODO:
      const todo = new Todo(action.texto);
      return [...state, todo];

    case ActionesTodo.SELECIONAR_ALL_TODO:
      return state.map(todoAll => {
          return{
            ...todoAll,
            completado: action.completado
          };
      });

    case ActionesTodo.SELECIONAR_TODO:
      return state.map(todoEdit => {
        if(todoEdit.id === action.id){
          return {
            ...todoEdit,
            completado: !todoEdit.completado
          };
        }else{
          return todoEdit;
        }
      });

    case ActionesTodo.EDITAR_TODO:
      return state.map(todoEdit => {
        if(todoEdit.id === action.id){
          return {
            ...todoEdit,
            texto: action.texto
          };
        }else{
          return todoEdit;
        }
      });

    case ActionesTodo.BORRAR_TODO:
      return state.filter( todoEdit => todoEdit.id !== action.id);

    case ActionesTodo.BORRAR_ALL_TODO:
      return state.filter( todoEdit => !todoEdit.completado);


    default:
      return state;
  }

}
