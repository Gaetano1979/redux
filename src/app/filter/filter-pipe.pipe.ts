import { Pipe, PipeTransform } from '@angular/core';
import { Todo } from '../reducers/todo.model';
import * as fromFilter from './../reducers/filter/filter.action';

@Pipe({
  name: 'filterPipe'
})
export class FilterPipePipe implements PipeTransform {

  transform(todos: Todo[], filtro: fromFilter.filtrosValidos): Todo[] {

    console.log(todos);

    console.log(filtro);
    switch (filtro) {
      case 'Completados':
        return todos.filter(todo => todo.completado);
      case 'Pendientes':
        return todos.filter(todo => !todo.completado);

      default:
        return todos;
    }
  }

}
